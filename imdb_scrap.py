import requests
import timeit
import time
import os
import re
import csv
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

# Input
print('IMDB Review scrapper.')
url = input('Paste url here : ')

# Start runtime
start_time = timeit.default_timer()

# Soup review link
soup = BeautifulSoup(requests.get(url).content, 'html5lib')

# Find title
for index in soup.find_all('a', {'itemprop' : 'url'}):
    tmp = str(index.text).split()
    break
title = ' '.join(tmp)
print('Title : ' + str(title))

# Calculate maxclicks from total review
for index in soup.find_all('div', {'class' : 'header'}):
    tmp = str(index.text).split()
    total = tmp[0].replace(',','')
    break
maxclick = int(total)//25
print('Total review : ' + total + ' review')
print('Total : ' + str(maxclick) + ' click')

# Webdriver
driver = webdriver.Chrome('chromedriver.exe')
wait = WebDriverWait(driver, 100)
driver.get(url)

# Click 'Load More' button
clicks = 0
while clicks <= int(maxclick):
    clicks += 1
    load_more_button  = wait.until(ec.visibility_of_element_located((By.CLASS_NAME, 'ipl-load-more__button'))).click()
    if clicks == int(maxclick):
        break

# Chek if review positive or negative
pos_word = []
for line in open('positive_words.txt', 'r'):
    pos_word.append(line.strip())
neg_word = []
for line in open('negative_words.txt', 'r'):
    neg_word.append(line.strip())

def pos_neg_check(text):
    pos_count = 0
    neg_count = 0
    result = ''

    text_split = text.split(' ')

    for word in text_split:
        for pos in pos_word:
            if pos == word:
                pos_count += 1
        for neg in neg_word:
            if neg == word:
                neg_count += 1
    print('Positive : ' + str(pos_count))
    print('Negative : ' + str(neg_count))

    if pos_count > neg_count:
        result = '1'
    elif pos_count == neg_count:
        result = '0'
    else:
        result = '-1'

    if result == '1':
        print('Result : Positive Review')
    elif result == '0':
        print('Result : Neutral Review')
    else:
        print('Result : Negative Review')

    return result

# Soup after automation click
filename = str(title).replace(' ', '_') + '.csv'
soup = BeautifulSoup(driver.page_source, 'html5lib')
if os.path.exists(filename):
    os.remove(filename)
for index in soup.find_all('div', {'class' : 'text show-more__control clickable'}):
    print(str(index.text))
    tmp = str(index.text).lower()
    tmp = re.sub('[^A-Za-z0-9]+', ' ', tmp)
    result = pos_neg_check(tmp)
    print(str(tmp) + '+' + str(result), file = open(filename, 'a', encoding = 'utf-8'))
for index in soup.find_all('div', {'class' : 'text show-more__control'}):
    print(str(index.text))
    tmp = str(index.text).lower()
    tmp = re.sub('[^A-Za-z0-9]+', ' ', tmp)
    result = pos_neg_check(tmp)
    print(str(tmp) + '+' + str(result), file = open(filename, 'a', encoding = 'utf-8'))

# End runtime
stop_time =  timeit.default_timer()

# Close webdriver
driver.close()

print('Time : ' + str(stop_time - start_time))
